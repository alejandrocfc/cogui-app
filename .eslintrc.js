module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    "plugin:react/recommended",
    "plugin:react-hooks/recommended",
    "standard",
    "plugin:prettier/recommended",
  ],
  overrides: [],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  plugins: ["react", "react-hooks"],
  rules: {
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "react/react-in-jsx-scope": "off",
    "react/display-name": 0,
    "react/prop-types": [
      "error",
      {
        ignore: ["children", "match", "route"],
      },
    ],
    "no-console": "off",
    semi: [2, "always"],
    "react/jsx-closing-bracket-location": [2, "tag-aligned"],
    "react/jsx-handler-names": 1,
  },
};
