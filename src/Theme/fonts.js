export default {
  Roboto: "Roboto",
  RobotoMedium: "Roboto_medium",
  GothamBook: "GothamBook",
  GothamMedium: "GothamMedium",
  GothamBold: "GothamBold",
  GothamBlack: "GothamBlack",
};
