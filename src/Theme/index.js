import FontsBase from "./fonts";
import FormsBase from "./form";

export const Fonts = FontsBase;
export const Forms = FormsBase;

export default {
  Fonts,
  Forms,
};
