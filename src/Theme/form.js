import Fonts from "./fonts";

export default {
  defaultPrimaryButtonText: {
    fontFamily: Fonts.RobotoMedium,
    color: "#f4f5f8",
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0.2,
  },
  defaultSecondaryButtonText: {
    fontFamily: Fonts.RobotoMedium,
    fontSize: 14,
    lineHeight: 16,
    letterSpacing: 0.2,
  },
};
