import pdfMake from "pdfmake/build/pdfmake";
import { convertDate2Age, formatDate } from "./mapper";
import { examOptions, formsText } from "./variables";

pdfMake.fonts = {
  Roboto: {
    normal:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Regular.ttf",
    bold: "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Medium.ttf",
    italics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Italic.ttf",
    bolditalics:
      "https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-MediumItalic.ttf",
  },
};

function getImage(url) {
  return new Promise((resolve, reject) => {
    const img = new Image();
    img.setAttribute("crossOrigin", "anonymous");

    img.onload = () => {
      const canvas = document.createElement("canvas");
      canvas.width = img.width;
      canvas.height = img.height;

      const ctx = canvas.getContext("2d");
      ctx.drawImage(img, 0, 0);

      const dataURL = canvas.toDataURL("image/png");

      resolve(dataURL);
    };

    img.onerror = (error) => {
      reject(error);
    };

    img.src = url;
  });
}

export const exportPet = async ({ owner, pet }, citas) => {
  const docDefinition = {
    content: [
      {
        alignment: "right",
        image: await getImage(`${window.location.origin}/icon.png`),
        width: 100,
      },
      {
        alignment: "center",
        style: "header",
        text: "HISTORIA CLINICA",
      },
      {
        style: "table",
        table: {
          widths: [60, "*", "auto", "auto"],
          headerRows: 1,
          body: [
            [
              {
                text: "DATOS PROPIETARIO",
                style: "tableHeader",
                colSpan: 4,
              },
              {},
              {},
              {},
            ],
            [
              { alignment: "center", style: "stepTitle", text: "NOMBRE" },
              {
                alignment: "center",
                style: "stepContent",
                text: owner.name,
                colSpan: 3,
              },
              {},
              {},
            ],
            [
              { alignment: "center", style: "stepTitle", text: "DIRECCIÓN" },
              {
                alignment: "center",
                style: "stepContent",
                text: owner.address,
              },
              { alignment: "center", style: "stepTitle", text: "TELÉFONO" },
              { alignment: "center", style: "stepContent", text: owner.tel },
            ],
          ],
        },
      },
      {
        style: "table",
        table: {
          widths: [60, "*", 60, "*"],
          headerRows: 1,
          body: [
            [
              {
                text: "DATOS PACIENTE",
                style: "tableHeader",
                colSpan: 4,
              },
              {},
              {},
              {},
            ],
            [
              { text: "NOMBRE", style: "stepTitle", alignment: "center" },
              {
                alignment: "center",
                colSpan: 3,
                style: "stepContent",
                text: pet.name,
              },
              {},
              {},
            ],
            [
              { text: "ESPECIE", style: "stepTitle", alignment: "center" },
              { text: pet.kind, style: "stepContent", alignment: "center" },
              { text: "RAZA", style: "stepTitle", alignment: "center" },
              { text: pet.breed, style: "stepContent", alignment: "center" },
            ],
            [
              { text: "EDAD", style: "stepTitle", alignment: "center" },
              {
                text: convertDate2Age(pet.age),
                style: "stepContent",
                alignment: "center",
              },
              { text: "SEXO", style: "stepTitle", alignment: "center" },
              { text: pet.sex, style: "stepContent", alignment: "center" },
            ],
          ],
        },
      },
      {
        alignment: "center",
        style: "header",
        text: "CITAS",
      },
    ],
    styles: {
      header: {
        bold: true,
        fontSize: 12,
        margin: [0, 0, 0, 10],
      },
      table: {
        margin: [0, 8],
      },
      content: {
        fontSize: 10,
      },
      tableHeader: {
        bold: true,
        fontSize: 12,
      },
      stepTitle: {
        bold: true,
        fontSize: 10,
      },
      stepContent: {
        fontSize: 10,
      },
    },
  };
  if (citas.length > 0) {
    const citasContent = generateCitasContent(citas);
    docDefinition.content.push(citasContent);
  }
  pdfMake.createPdf(docDefinition).download();
};
export const exportAppointment = async (cita, { owner, pet }) => {
  const citaType = cita.type === "appointment" ? "Consulta" : "Fisio";
  const docDefinition = {
    content: [
      {
        alignment: "right",
        image: await getImage(`${window.location.origin}/icon.png`),
        width: 100,
      },
      {
        alignment: "center",
        style: "header",
        text: `CITA - ${citaType}`,
      },
      {
        style: "content",
        text: `Fecha ${formatDate(cita.date)}`,
        margin: [0, 10],
      },
      {
        style: "table",
        table: {
          widths: [60, "*", "auto", "auto"],
          headerRows: 1,
          body: [
            [
              {
                text: "DATOS PROPIETARIO",
                style: "tableHeader",
                colSpan: 4,
              },
              {},
              {},
              {},
            ],
            [
              { alignment: "center", style: "stepTitle", text: "NOMBRE" },
              {
                alignment: "center",
                style: "stepContent",
                text: owner.name,
                colSpan: 3,
              },
              {},
              {},
            ],
            [
              { alignment: "center", style: "stepTitle", text: "DIRECCIÓN" },
              {
                alignment: "center",
                style: "stepContent",
                text: owner.address,
              },
              { alignment: "center", style: "stepTitle", text: "TELÉFONO" },
              { alignment: "center", style: "stepContent", text: owner.tel },
            ],
          ],
        },
      },
      {
        style: "table",
        table: {
          widths: [60, "*", 60, "*"],
          headerRows: 1,
          body: [
            [
              {
                text: "DATOS PACIENTE",
                style: "tableHeader",
                colSpan: 4,
              },
              {},
              {},
              {},
            ],
            [
              { text: "NOMBRE", style: "stepTitle", alignment: "center" },
              {
                alignment: "center",
                colSpan: 3,
                style: "stepContent",
                text: pet.name,
              },
              {},
              {},
            ],
            [
              { text: "ESPECIE", style: "stepTitle", alignment: "center" },
              { text: pet.kind, style: "stepContent", alignment: "center" },
              { text: "RAZA", style: "stepTitle", alignment: "center" },
              { text: pet.breed, style: "stepContent", alignment: "center" },
            ],
            [
              { text: "EDAD", style: "stepTitle", alignment: "center" },
              {
                text: convertDate2Age(pet.age),
                style: "stepContent",
                alignment: "center",
              },
              { text: "SEXO", style: "stepTitle", alignment: "center" },
              { text: pet.sex, style: "stepContent", alignment: "center" },
            ],
          ],
        },
      },
    ],
    styles: {
      header: {
        bold: true,
        fontSize: 12,
        margin: [0, 0, 0, 10],
      },
      table: {
        margin: [0, 8],
      },
      content: {
        fontSize: 10,
      },
      tableHeader: {
        bold: true,
        fontSize: 12,
      },
      stepTitle: {
        bold: true,
        fontSize: 10,
      },
      stepContent: {
        fontSize: 10,
      },
    },
  };
  if (cita.type === "physio") {
    docDefinition.content.push({
      style: "table",
      table: {
        widths: ["100%"],
        body: [
          [{ style: "tableHeader", text: "SEGUIMIENTO" }],
          [{ style: "content", text: cita.description }],
        ],
      },
    });
  } else {
    const appointmentContent = generateAppointmentContent(cita.data);
    docDefinition.content.push(...appointmentContent);
  }
  if (cita.note) {
    docDefinition.content.push({
      style: "table",
      table: {
        widths: [60, "*", "auto", "*"],
        body: [
          {
            stack: [
              { text: "COMENTARIOS", style: "stepTitle" },
              { text: cita.note, style: "stepContent" },
            ],
            colSpan: 4,
          },
          {},
          {},
          {},
        ],
      },
    });
  }

  pdfMake.createPdf(docDefinition).download();
};

const generateAppointmentContent = ({ appointment, exam }) => {
  const content = [
    {
      style: "table",
      table: {
        widths: ["100%"],
        body: [
          [{ style: "tableHeader", text: "ANAMNESIS" }],
          [{ style: "content", text: appointment.anamnesis }],
        ],
      },
    },
  ];
  if (Object.values(exam).flat(1).length > 0) {
    let _exam = Object.keys(exam).map((key) => {
      const arr = {
        title: formsText[key].toUpperCase(),
        labels: [],
        values: [],
      };
      examOptions[key].forEach(({ label }) => {
        arr.labels.push(label);
        arr.values.push(exam[key].includes(label) ? "X" : "");
      });
      return arr;
    });
    _exam = formsText.order.exam.map((key) =>
      _exam.find((i) => i.title === key)
    );
    const contentExam = [
      [
        { colSpan: 9, style: "tableHeader", text: "REGIÓN ANATÓMICA AFECTADA" },
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
      ],
      ..._exam.map((item) => {
        const row = [{ style: "tableHeader", text: item.title }];
        item.labels.forEach((_, index) => {
          row.push({
            style: "tableHeader",
            text: item.labels[index],
            bold: false,
          });
          row.push({ style: "tableHeader", text: item.values[index] });
        });
        return row;
      }),
    ];
    content.push(
      {
        style: "tableHeader",
        decoration: "underline",
        text: "EXÁMEN CLÍNICO",
      },
      {
        style: "table",
        table: {
          headerRows: 1,
          widths: ["*", 40, 20, 40, 20, 40, 20, 40, 20],
          body: [...contentExam],
        },
      }
    );
  }
  content.push(
    {
      style: "table",
      table: {
        headerRows: 1,
        widths: ["100%"],
        body: [
          [{ style: "tableHeader", text: "DESCRIPCIÓN DE SIGNOS" }],
          [{ style: "content", text: appointment.signs }],
        ],
      },
    },
    {
      style: "table",
      table: {
        headerRows: 1,
        widths: ["100%"],
        body: [
          [{ style: "tableHeader", text: "TRATAMIENTO" }],
          [{ style: "content", text: appointment.treatment }],
        ],
      },
    }
  );
  return content;
};

const generateCitasContent = (citas) => {
  const content = [];
  citas.forEach((cita) => {
    const citaType = cita.type === "appointment" ? "Consulta" : "Fisioterapia";
    content.push(
      {
        style: "tableHeader",
        decoration: "underline",
        text: citaType,
      },
      {
        style: "tableHeader",
        text: `Fecha ${formatDate(cita.date)}`,
      }
    );
    if (cita.type === "physio") {
      content.push({
        style: "table",
        table: {
          widths: ["100%"],
          body: [
            [{ style: "tableHeader", text: "SEGUIMIENTO" }],
            [{ style: "content", text: cita.description }],
          ],
        },
      });
    } else {
      const appointmentContent = generateAppointmentContent(cita.data);
      content.push(appointmentContent);
    }
  });
  return content;
};
