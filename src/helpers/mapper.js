import { formsText } from "./variables";

const monthNames = [
  "Ene.",
  "Feb.",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Sept.",
  "Oct.",
  "Nov.",
  "Dic.",
];
export const cleanEmptyFields = (obj) =>
  Object.fromEntries(Object.entries(obj).filter(([, v]) => !!v));

export const mapperTitle = (values) =>
  Object.keys(values).map((key) => ({
    title: formsText[key],
    fields: formsText.order[key].map((field) => ({
      label: formsText[field],
      value: isADateInstance(values[key][field])
        ? convertDate2Age(values[key][field])
        : values[key][field],
    })),
  }));

export const isADateInstance = (date) => date instanceof Date && !isNaN(date);

export const formatAMPM = (date) => {
  const dateObj = isADateInstance(date) ? date : new Date(date.toDate());
  let hours = dateObj.getHours();
  let minutes = dateObj.getMinutes();
  const ampm = hours >= 12 ? "pm" : "am";
  hours %= 12;
  hours = hours !== 0 ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? `0${minutes}` : minutes;
  return ` ${hours}:${minutes} ${ampm}`;
};
export const formatDate = (input) => {
  const date = isADateInstance(input) ? input : new Date(input.toDate());
  return `${
    monthNames[date.getMonth()]
  } ${date.getDate()}, ${date.getFullYear()} ${formatAMPM(date)}`;
};
export const convertDate2Age = (date, endDate = new Date()) => {
  if (!date) {
    return "";
  }
  const startDate = isADateInstance(date) ? date : new Date(date.toDate());
  const totalMonths =
    endDate.getMonth() -
    startDate.getMonth() +
    12 * (endDate.getFullYear() - startDate.getFullYear());
  const getYear = () => {
    const year = Math.trunc(totalMonths / 12);
    if (year < 1) {
      return "";
    }
    if (year < 2) {
      return `${year} año `;
    }
    if (year > 1) {
      return `${year} años `;
    }
  };
  const getMonth = () => {
    const month = totalMonths % 12;
    if (month < 1) {
      return "";
    }
    if (month < 2) {
      return `${month} mes`;
    }
    if (month > 1) {
      return `${month} meses`;
    }
  };
  return getYear() + getMonth();
};
export const getShortDate = (date = new Date()) => {
  const date2Convert = isADateInstance(date) ? date : new Date(date.toDate());
  return `${date2Convert.getDate()} de ${monthNames[
    date2Convert.getMonth()
  ].toLowerCase()} del ${date2Convert.getFullYear()}`;
};
export const formatYearMonth = (date = new Date()) => {
  const months = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ];
  const monthName = months[date.getMonth()];
  return `${monthName} de ${date.getFullYear()}`;
};
export const setTimeDate2Date = (selectedDate, selectedTime) => {
  const day = selectedDate.getDate();
  const month = selectedDate.getMonth();
  const year = selectedDate.getFullYear();
  const hours = selectedTime.getHours();
  const minutes = selectedTime.getMinutes();
  const newDate = new Date();
  newDate.setDate(day);
  newDate.setMonth(month);
  newDate.setFullYear(year);
  newDate.setHours(hours);
  newDate.setMinutes(minutes);
  return newDate;
};

export const isPastDate = (
  selectedDate,
  selectedTime,
  currentDate = new Date()
) => {
  const newDate = setTimeDate2Date(selectedDate, selectedTime);
  return currentDate.getTime() > newDate.getTime();
};

export const getTimeName = () => {
  let timeName = "Buenas noches,";
  const hours = new Date().getHours();
  if (hours < 12) {
    timeName = "Buenos días,";
  } else if (hours < 18) {
    timeName = "Buenas tardes,";
  }
  return timeName;
};
export const generateId = () =>
  Array(28)
    .join(`${Math.random().toString(36)}00000000000000000`.slice(2, 18))
    .slice(0, 28);

export const checkNested = (obj, level, ...rest) => {
  if (obj === undefined) {
    return false;
  }
  if (rest.length === 0 && Object.prototype.hasOwnProperty.call(obj, level)) {
    return true;
  }
  return checkNested(obj[level], ...rest);
};
