const formsText = {
  anamnesis: "anamnesis",
  signs: "Descripción de Signos",
  treatment: "Tratamiento",
  exam: "región anatómica afectada: ",
  owner: "esclavo",
  pet: "mascota",
  name: "nombre",
  address: "dirección",
  document: "documento",
  tel: "teléfono",
  age: "edad",
  breed: "raza",
  kind: "especie",
  sex: "sexo",
  order: {
    exam: ["COLUMNA", "MIEMBROS", "SENSIBILIDAD", "PROPIOCEPCIÓN"],
    owner: ["name", "tel", "address"],
    pet: ["name", "kind", "breed", "sex", "age"],
  },
  spine: "columna",
  limb: "miembros",
  sensibility: "sensibilidad",
  kinaesthesia: "propiocepción",
};
const examOptions = {
  spine: [
    { label: "CERV", value: "CERV" },
    { label: "TORA", value: "TORA" },
    { label: "LUMB", value: "LUMB" },
    { label: "SACR", value: "SACR" },
  ],
  limb: [
    { label: "MAD", value: "MAD" },
    { label: "MAI", value: "MAI" },
    { label: "MPD", value: "MPD" },
    { label: "MPI", value: "MPI" },
  ],
  sensibility: [
    { label: "MAD", value: "MAD" },
    { label: "MAI", value: "MAI" },
    { label: "MPD", value: "MPD" },
    { label: "MPI", value: "MPI" },
  ],
  kinaesthesia: [
    { label: "MAD", value: "MAD" },
    { label: "MAI", value: "MAI" },
    { label: "MPD", value: "MPD" },
    { label: "MPI", value: "MPI" },
  ],
};
const colorPicker = [
  { value: "tomato", label: "#D50000" },
  { value: "flamingo", label: "#E67C73" },
  { value: "tangerine", label: "#F4511E" },
  { value: "banana", label: "#F6BF26" },
  { value: "sage", label: "#33B679" },
  { value: "basil", label: "#0B8043" },
  { value: "peacock", label: "#039BE5" },
  { value: "blueberry", label: "#3F51B5" },
  { value: "lavender", label: "#7986CB" },
  { value: "grape", label: "#8E24AA" },
  { value: "graphite", label: "#616161" },
];
const dateOptions = [
  { value: "appointment", label: "Consulta" },
  { value: "physio", label: "Fisio" },
  { value: "acupunture", label: "Acupuntura" },
];
const typeDatesHome = {
  TODAY: "TODAY",
  ACTIVE: "ACTIVE",
};

module.exports = {
  colorPicker,
  dateOptions,
  examOptions,
  formsText,
  typeDatesHome,
};
