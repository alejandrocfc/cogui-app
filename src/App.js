import React from "react";
import { BrowserRouter } from "react-router-dom";
import { NativeBaseProvider } from "native-base";
import Notification from "./molecules/Notification";
import Authorization from "./molecules/Authorization";

function App() {
  return (
    <BrowserRouter>
      <NativeBaseProvider>
        <Notification />
        <Authorization />
      </NativeBaseProvider>
    </BrowserRouter>
  );
}

export default App;
