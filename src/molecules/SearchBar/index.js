import React from "react";
import { Box, Icon, Input, Pressable, Skeleton, VStack } from "native-base";
import { MdClose, MdSearch } from "react-icons/md";
import { Fonts } from "../../Theme";
import { bool, func, string } from "prop-types";

function SearchBar({ handleChange, handleClear, loader, value }) {
  return (
    <>
      <VStack w="100%" maxW="300px" space={5} alignSelf="center">
        <Input
          _focus={{ bg: "transparent" }}
          _input={{
            selectionColor: "#dbf4ff",
            cursorColor: "#004282",
            fontSize: 14,
            color: "#27272a",
            fontFamily: Fonts.GothamBook,
          }}
          placeholder="Buscar por nombre"
          focusOutlineColor="darkBlue.700"
          width="100%"
          py="3"
          px="1"
          value={value}
          onChangeText={handleChange}
          InputLeftElement={
            <Box fontSize="21" m="2" ml="3" color="gray.400">
              <Icon as={<MdSearch />} />
            </Box>
          }
          InputRightElement={
            value && (
              <Pressable
                fontSize="21"
                m="2"
                mr="3"
                color="gray.400"
                onPress={handleClear}
              >
                <Icon as={<MdClose />} />
              </Pressable>
            )
          }
        />
      </VStack>
      {loader && (
        <Skeleton h="3" flex="1" rounded="full" startColor="indigo.300" />
      )}
    </>
  );
}
SearchBar.propTypes = {
  handleChange: func,
  handleClear: func,
  loader: bool,
  value: string,
};

export default SearchBar;
