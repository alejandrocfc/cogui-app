import React from "react";
import { bool, func, node, string } from "prop-types";
import { Box, ScrollView } from "native-base";
import AppBar from "../../atoms/AppBar";

function Layout({ children, footer, isForm, onPress, rightComp, title }) {
  return (
    <Box height="100vh" display="flex" bg="orange.100">
      <AppBar>
        <AppBar.Back onPress={onPress} />
        <AppBar.Title title={title} />
        {rightComp}
      </AppBar>
      {isForm ? (
        <Box mt="3" px="4" flex="1 1 0%">
          {children}
        </Box>
      ) : (
        <ScrollView mt="3" px="3" h="100%">
          {children}
        </ScrollView>
      )}
      {footer}
    </Box>
  );
}
Layout.defaultProps = { footer: null, isForm: false, rightComp: null };
Layout.propTypes = {
  onPress: func.isRequired,
  footer: node,
  isForm: bool,
  rightComp: node.isRequired,
  title: string.isRequired,
};

export default Layout;
