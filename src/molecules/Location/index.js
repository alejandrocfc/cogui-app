import { Box, Button } from "native-base";
import React, { useState } from "react";
import { InputLabel } from "../../atoms/Input";

function Location() {
  const [location, setLocation] = useState({});
  const getLocation = async () => {
    setLocation("asdasd");
    /* const res = await requestLocation();
    if (res) {
      Geolocation.getCurrentPosition(
        position => {
          setLocation(position.coords);
          addValue('owner.latitude', position.coords.latitude);
          addValue('owner.longitude', position.coords.longitude);
          toast.show({title: 'Ubicación obtenida'});
        },
        err => {
          console.log(err);
          toast.show({status: 'error', title: 'Error al obtener la ubicación'});
        },
      );
    } else {
      toast.show({
        status: 'error',
        title: 'No se puede acceder a la ubicación',
      });
    } */
  };
  const showLocation = () => {
    // const {latitude, longitude} = location;
    console.log(location);
    // Linking.openURL(`geo:0,0?q=${latitude},${longitude}`);
  };
  return (
    <Box mt="4">
      <InputLabel>Ubicación</InputLabel>
      {location.latitude ? (
        <Button.Group isAttached space={6} mx={{ base: "auto", md: 0 }}>
          <Button onPress={showLocation} variant="link">
            Ver ubicación
          </Button>
          <Button onPress={getLocation} variant="link">
            Actualizar ubicación
          </Button>
        </Button.Group>
      ) : (
        <Button onPress={getLocation} variant="link">
          Obtener ubicación
        </Button>
      )}
    </Box>
  );
}

export default Location;
