import React from "react";
import { shape, func, arrayOf } from "prop-types";
import {
  Box,
  ChevronRightIcon,
  HStack,
  Pressable,
  Spacer,
  Stack,
  VStack,
} from "native-base";
import { Description, Title } from "../../atoms/Text";

function ListItem({ item, onPress }) {
  return (
    <Pressable onPress={() => onPress(item.id)}>
      <Box
        borderBottomWidth="1"
        _dark={{
          borderColor: "muted.50",
        }}
        borderColor="muted.800"
        pl={["0", "4"]}
        pr={["0", "5"]}
      >
        <Stack p="4" space={3}>
          <HStack space={[2, 3]} justifyContent="space-between">
            <VStack>
              <Title text={item.title} />
              <Description text={item.subtitle} />
            </VStack>
            <Spacer />
            <ChevronRightIcon size="5" mt="5" color="darkBlue.700" />
          </HStack>
        </Stack>
      </Box>
    </Pressable>
  );
}
ListItem.defaultProps = {
  item: {},
  onPress: () => undefined,
};
ListItem.propTypes = {
  item: shape({}),
  onPress: func,
};
function List({ data, onRowPress }) {
  return (
    <Box pb="30">
      {data.map((item, key) => (
        <ListItem key={String(key)} item={item} onPress={onRowPress} />
      ))}
    </Box>
  );
}

List.propTypes = {
  data: arrayOf(shape({})).isRequired,
  onRowPress: func.isRequired,
};

export default List;
