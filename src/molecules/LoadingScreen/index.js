import React from "react";
import { Center, Container, VStack } from "native-base";
import Loading from "../../atoms/Loading";
import { Title } from "../../atoms/Text";

function LoadingScreen() {
  return (
    <Center>
      <Container>
        <Center flex={1} px="3">
          <VStack space={2} alignItems="center">
            <Loading />
            <Title text="Cargando" />
          </VStack>
        </Center>
      </Container>
    </Center>
  );
}

export default LoadingScreen;
