/* eslint-disable */
import {Navigate, NavLink, Outlet, Route, Routes, useParams} from "react-router-dom";
import { useEffect, useState } from "react";
import { MdPets, MdHome } from "react-icons/md";
import { Box, Center, HStack, Icon } from "native-base";
import { VIEWER } from "../../pages/Authorization/roles";
import { useCoguiContext } from "../../context";
import {
  OWNERS_SET,
  PETS_SET,
  USER,
  USER_LOGIN_IN,
  USER_LOGIN_OUT,
} from "../../context/types";
import AuthProvider from "../../firecogui/auth";
import { onAuthStateChanged } from "firebase/auth";
import LoadingScreen from "../LoadingScreen";
import Login from "../../pages/Login";
import Home from "../../pages/Home";
import Pets from "../../pages/Pets";
import { Description } from "../../atoms/Text";
import { collection, onSnapshot, orderBy } from "firebase/firestore";
import { db } from "../../firecogui/db";
import PetForm from "../../pages/Pets/form";

function Authorization() {
  const [controller, dispatch] = useCoguiContext();
  const [loading, setLoading] = useState(true);
  const isAuthorized = controller.role === VIEWER;

  useEffect(() => {
    if (AuthProvider.auth.currentUser) {
      dispatch({ type: USER, value: AuthProvider.auth.currentUser });
      dispatch({ type: USER_LOGIN_IN });
      setLoading(false);
    } else {
      onAuthStateChanged(
        AuthProvider.auth,
        (user) => {
          if (user) {
            dispatch({ type: USER, value: user });
            dispatch({ type: USER_LOGIN_IN });
            setLoading(false);
          } else {
            dispatch({ type: USER_LOGIN_OUT });
            setLoading(false);
          }
        },
        () => {
          dispatch({ type: USER_LOGIN_OUT });
          setLoading(false);
        }
      );
    }
  }, []);
  if (loading) return <LoadingScreen />;
  return (
    <Routes>
      <Route path="/login" element={<Login />} />
      <Route
        index
        element={<Navigate to={isAuthorized ? "/cogui" : "/login"} />}
      />
      <Route element={<Layout />}>
        <Route path="/cogui" element={<Home />} />
        <Route path="/pacientes" element={<Pets />} />
      </Route>
      <Route path="/formulario-paciente" element={<PetForm />} />
      <Route path="/paciente/:id" element={<Asd />} />
      <Route path="/formulario-cita" element={<Test />} />
      <Route path="/cita/:id" element={<Test />} />
      <Route path="/editar-cita" element={<Test />} />
      <Route path="FileInput" element={<Test />} />
      <Route path="/formula" element={<Test />} />
    </Routes>
  );
}
const Test = () => <h1>hOLA</h1>;
const Asd = () => {
  const params = useParams();
  return <h1>hOLA {params.id}</h1>;
}

const Layout = () => {
  const [controller, dispatch] = useCoguiContext();
  const [hasData, setData] = useState(false);
  useEffect(() => {
    const unsubPet = onSnapshot(
      collection(db, "pets"),
      orderBy("name", "asc"),
      (querySnapshot) => {
        if (!querySnapshot.empty) {
          const pets = [];
          querySnapshot.forEach((doc) => {
            pets.push({ id: doc.id, ...doc.data() });
          });
          dispatch({
            type: PETS_SET,
            value: pets,
          });
        }
      },
      (error) => console.log(error)
    );
    const unsubOwners = onSnapshot(
      collection(db, "owners"),
      (querySnapshot) => {
        if (!querySnapshot.empty) {
          const slaves = [];
          querySnapshot.forEach((doc) => {
            slaves.push({ id: doc.id, ...doc.data() });
          });
          dispatch({
            type: OWNERS_SET,
            value: slaves,
          });
        }
      },
      (error) => console.log(error)
    );
    return () => {
      unsubPet();
      unsubOwners();
    };
  }, []);

  useEffect(() => {
    if (controller.pets.length > 0 && controller.owners.length > 0) {
      setData(true);
    }
  }, [controller.pets, controller.owners]);

  if (!hasData) {
    return <LoadingScreen />;
  }
  return (
    <Box height="100vh" display="flex" bg="orange.100">
      <Box flex="1 1 0%">
        <Outlet />
      </Box>
      <Box h="16" safeAreaTop width="100%" alignSelf="center">
        <HStack
          bg="white"
          borderTopRightRadius={20}
          borderTopLeftRadius={20}
          alignItems="center"
          safeAreaBottom
          shadow="9"
        >
          <Box py="3" flex={1}>
            <NavLink style={{ textDecoration: "none" }} to="/cogui">
              {({ isActive }) => (
                <Center
                  fontSize={isActive ? "22" : "18"}
                  color={isActive ? "secondary.600" : "rose.300"}
                >
                  <Icon as={<MdHome />} />
                  <Description
                    text="Inicio"
                    color={isActive ? "secondary.600" : "rose.300"}
                  />
                </Center>
              )}
            </NavLink>
          </Box>
          <Box py="2" flex={1}>
            <NavLink style={{ textDecoration: "none" }} to="/pacientes">
              {({ isActive }) => (
                <Center
                  fontSize={isActive ? "22" : "18"}
                  color={isActive ? "secondary.600" : "rose.300"}
                >
                  <Icon as={<MdPets />} />
                  <Description
                    text="Pacientes"
                    color={isActive ? "secondary.600" : "rose.300"}
                  />
                </Center>
              )}
            </NavLink>
          </Box>
        </HStack>
      </Box>
    </Box>
  );
};

export default Authorization;
