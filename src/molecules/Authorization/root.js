import { NavLink, Outlet, Route } from "react-router-dom";
import Home from "../../pages/Home";
import Pets from "../../pages/Pets";
import { useCoguiContext } from "../../context";
import { useEffect, useState } from "react";
import { collection, onSnapshot, orderBy } from "firebase/firestore";
import { db } from "../../firecogui/db";
import { OWNERS_SET, PETS_SET } from "../../context/types";
import LoadingScreen from "../LoadingScreen";
import { Box, Center, HStack, Icon } from "native-base";
import { MdHome, MdPets } from "react-icons/md";
import { Description } from "../../atoms/Text";

const Layout = () => {
  const [controller, dispatch] = useCoguiContext();
  const [hasData, setData] = useState(false);
  useEffect(() => {
    const unsubPet = onSnapshot(
      collection(db, "pets"),
      orderBy("name", "asc"),
      (querySnapshot) => {
        if (!querySnapshot.empty) {
          const pets = [];
          querySnapshot.forEach((doc) => {
            pets.push({ id: doc.id, ...doc.data() });
          });
          dispatch({
            type: PETS_SET,
            value: pets,
          });
        }
      },
      (error) => console.log(error)
    );
    const unsubOwners = onSnapshot(
      collection(db, "owners"),
      (querySnapshot) => {
        if (!querySnapshot.empty) {
          const slaves = [];
          querySnapshot.forEach((doc) => {
            slaves.push({ id: doc.id, ...doc.data() });
          });
          dispatch({
            type: OWNERS_SET,
            value: slaves,
          });
        }
      },
      (error) => console.log(error)
    );
    return () => {
      unsubPet();
      unsubOwners();
    };
  }, []);

  useEffect(() => {
    if (controller.pets.length > 0 && controller.owners.length > 0) {
      setData(true);
    }
  }, [controller.pets, controller.owners]);

  if (!hasData) {
    return <LoadingScreen />;
  }
  return (
    <Box height="100vh" display="flex" bg="orange.100">
      <Box flex="1 1 0%">
        <Outlet />
      </Box>
      <Box h="16" safeAreaTop width="100%" alignSelf="center">
        <HStack
          bg="white"
          borderTopRightRadius={20}
          borderTopLeftRadius={20}
          alignItems="center"
          safeAreaBottom
          shadow="9"
        >
          <Box py="3" flex={1}>
            <NavLink style={{ textDecoration: "none" }} to="/inicio">
              {({ isActive }) => (
                <Center
                  fontSize={isActive ? "22" : "18"}
                  color={isActive ? "secondary.600" : "rose.300"}
                >
                  <Icon as={<MdHome />} />
                  <Description
                    text="Inicio"
                    color={isActive ? "secondary.600" : "rose.300"}
                  />
                </Center>
              )}
            </NavLink>
          </Box>
          <Box py="2" flex={1}>
            <NavLink style={{ textDecoration: "none" }} to="/pacientes">
              {({ isActive }) => (
                <Center
                  fontSize={isActive ? "22" : "18"}
                  color={isActive ? "secondary.600" : "rose.300"}
                >
                  <Icon as={<MdPets />} />
                  <Description
                    text="Pacientes"
                    color={isActive ? "secondary.600" : "rose.300"}
                  />
                </Center>
              )}
            </NavLink>
          </Box>
        </HStack>
      </Box>
    </Box>
  );
};
const RootNavigation = () => {
  return (
    <Route path="/" element={<Cont />}>
      <Route element={<Layout />}>
        <Route path="/inicio" element={<Home />} />
        <Route path="/pacientes" element={<Pets />} />
      </Route>
      <Route path="/form" element={<Test />} />
    </Route>
  );
};

const Test = () => <h1>hOLA</h1>;
const Cont = () => (
  <div>
    <Outlet />
  </div>
);

export default RootNavigation;
