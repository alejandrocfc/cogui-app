import { string } from "prop-types";
import { Box, Heading, Stack } from "native-base";

function Card({ children, title }) {
  return (
    <Box alignItems="center" m={2}>
      <Box
        flex="1"
        w="100%"
        shadow="6"
        rounded="lg"
        borderColor="coolGray.200"
        backgroundColor="gray.50"
        borderWidth="1"
      >
        <Stack p="4" space={3}>
          {title && (
            <Heading color="secondary.600" size="sm" ml="-1">
              {title}
            </Heading>
          )}
          {children}
        </Stack>
      </Box>
    </Box>
  );
}

Card.defaultProps = {
  title: "",
};

Card.propTypes = {
  title: string,
};

export default Card;
