/* eslint-disable */
import React from "react";
import { Box, Icon } from "native-base";
import { MdPets, MdPeople } from "react-icons/md";
import { mapperTitle } from "../../helpers/mapper";
import { Paragraph } from "../../atoms/Text";
import Card from "../Card";
function SummaryStep({ values }) {
  const stepCount = 2;
  const [width, setWidth] = React.useState(0);
  const [height, setHeight] = React.useState(0);
  const data = mapperTitle(values);
  const renderProgressBarBackground = () => {
    const progressBarBackgroundStyle = {
      backgroundColor: "#0077E6",
      position: "absolute",
      left: (width - 3) / 2,
      top: height / (2 * stepCount),
      bottom: height / (2 * stepCount),
      width: 3,
    };
    return <div style={progressBarBackgroundStyle} />;
  };

  const renderStepIndicator = () => {
    return (
      <div style={styles.stepIndicatorContainer}>
        {data.map(({ title, ...form }, index) => (
          <div key={index} style={styles.stepContainer}>
            {renderStep(index)}
          </div>
        ))}
      </div>
    );
  };
  const renderStep = (position) => {
    const getIcon = [MdPets, MdPeople];
    return (
      <div key={"step-indicator"} style={styles.step}>
        <Icon as={getIcon[position]} size={20} color="#005DB4" />
      </div>
    );
  };

  return (
    <>
      <div style={styles.container}>
        {width !== 0 && (
          <>{renderProgressBarBackground()}</>
        )}
        {renderStepIndicator()}
        <Box {...styles.stepIndicatorContainer} w='85%'>
          {data.map(({ title, fields }, index) => (
            <Box w="100%" key={index}>
              <Card title={title}>
                {fields.map((field, key) => (
                  <Paragraph
                    key={key}
                    heading={field.label}
                    content={field.value}
                  />
                ))}
              </Card>
            </Box>
          ))}
        </Box>
      </div>
    </>
  );
}

const styles = {
  container: {
    flexDirection: "row",
    flex: 1,
  },
  stepIndicatorContainer: {
    flexDirection: "column",
    width: 40,
    alignItems: "center",
    justifyContent: "space-around",
    backgroundColor: "rgba(1,0,0,0)",
  },
  step: {
    alignItems: "center",
    backgroundColor: "#FAFAFA",
    borderWidth: 0,
    borderColor: "#FAFAFA",
    height: 40,
    width: 40,
    borderRadius: 20,
    overflow: "hidden",
    justifyContent: "center",
    zIndex: 2,
  },
  stepContainer: {
    position: "relative",
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
};

export default SummaryStep;
