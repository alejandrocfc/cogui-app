import React from 'react';
import {FormControl, WarningOutlineIcon} from 'native-base';
import {bool, oneOfType, shape, string} from 'prop-types';
import {Controller} from 'react-hook-form';
import {Input, InputLabel} from '../../atoms/Input';

function Inputcontrolled({
  control,
  controlledProps,
  defaultValue,
  disabled,
  hasError,
  inputProps,
  label,
  labelRequired,
  name,
  type,
  rules,
  setRef,
}) {
  return (
    <FormControl
      isInvalid={hasError}
      isDisabled={disabled}
      isRequired={labelRequired}
      {...controlledProps}>
      <InputLabel>{label}</InputLabel>
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={rules}
        render={({field: {onChange, value}, fieldState: {error}}) => (
          <>
            <Input
              type={type}
              ref={setRef}
              value={value}
              onChangeText={onChange}
              {...inputProps}
            />
            {!!error && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}>
                {error.message}
              </FormControl.ErrorMessage>
            )}
          </>
        )}
      />
    </FormControl>
  );
}

Inputcontrolled.defaultProps = {
  defaultValue: '',
  disabled: false,
  errorText: false,
  labelRequired: false,
  hasError: false,
  controlledProps: {},
  inputProps: {},
  rules: {},
  setRef: null,
  type: 'text',
};

Inputcontrolled.propTypes = {
  controlledProps: shape({}),
  control: shape({}).isRequired,
  hasError: bool,
  defaultValue: string,
  disabled: bool,
  errorText: oneOfType([string, bool]),
  inputProps: shape({}),
  label: string.isRequired,
  labelRequired: bool,
  name: string.isRequired,
  rules: shape({}),
  setRef: shape({}),
  type: string,
};

export default Inputcontrolled;
