import React from "react";
import { FormControl } from "native-base";
import { bool, shape, string } from "prop-types";
import { Controller } from "react-hook-form";
import { InputLabel } from "../../atoms/Input";

function DateControlled({
  control,
  controlledProps,
  datePickerProps,
  label,
  labelRequired,
  name,
}) {
  return (
    <FormControl isRequired={labelRequired} {...controlledProps}>
      <InputLabel>{label}</InputLabel>
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value } }) => (
          <input
            type="date"
            onChange={onChange}
            value={value}
            {...datePickerProps}
          />
        )}
      />
    </FormControl>
  );
}

DateControlled.defaultProps = {
  labelRequired: false,
  controlledProps: {},
  datePickerProps: {},
};

DateControlled.propTypes = {
  controlledProps: shape({}),
  datePickerProps: shape({}),
  control: shape({}).isRequired,
  label: string.isRequired,
  labelRequired: bool,
  name: string.isRequired,
};

export default DateControlled;
