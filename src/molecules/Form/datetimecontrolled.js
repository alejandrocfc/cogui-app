import React from 'react';
import {FormControl} from 'native-base';
import {bool, shape, string} from 'prop-types';
import DateTimePicker from '../DatePicker/datetime';
import {InputLabel} from '../../atoms/Input';

function DateTimeControlled({
  controlledProps,
  control,
  disabled,
  label,
  labelRequired,
}) {
  return (
    <FormControl
      isDisabled={disabled}
      isRequired={labelRequired}
      {...controlledProps}>
      <InputLabel>{label}</InputLabel>
      <DateTimePicker control={control} disabled={disabled} />
    </FormControl>
  );
}

DateTimeControlled.defaultProps = {
  labelRequired: false,
  controlledProps: {},
  disabled: false,
  datePickerProps: {},
};

DateTimeControlled.propTypes = {
  controlledProps: shape({}),
  datePickerProps: shape({}),
  disabled: bool,
  label: string.isRequired,
  labelRequired: bool,
};

export default DateTimeControlled;
