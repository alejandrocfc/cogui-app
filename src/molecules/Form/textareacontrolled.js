import React from 'react';
import {FormControl, WarningOutlineIcon} from 'native-base';
import {bool, oneOfType, shape, string} from 'prop-types';
import {Controller} from 'react-hook-form';
import {InputArea, InputLabel} from '../../atoms/Input';

function TextAreaControlled({
  control,
  controlledProps,
  defaultValue,
  disabled,
  hasError,
  inputProps,
  label,
  labelRequired,
  name,
  rules,
}) {
  return (
    <FormControl
      isInvalid={hasError}
      isDisabled={disabled}
      isRequired={labelRequired}
      {...controlledProps}>
      <InputLabel>{label}</InputLabel>
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={rules}
        render={({field: {onChange, value}, fieldState: {error}}) => (
          <>
            <InputArea value={value} onChangeText={onChange} {...inputProps} />
            {!!error && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}>
                {error.message}
              </FormControl.ErrorMessage>
            )}
          </>
        )}
      />
    </FormControl>
  );
}

TextAreaControlled.defaultProps = {
  defaultValue: '',
  disabled: false,
  errorText: false,
  labelRequired: false,
  hasError: false,
  controlledProps: {},
  inputProps: {},
  rules: {},
};

TextAreaControlled.propTypes = {
  controlledProps: shape({}),
  control: shape({}).isRequired,
  hasError: bool,
  defaultValue: string,
  disabled: bool,
  errorText: oneOfType([string, bool]),
  inputProps: shape({}),
  label: string.isRequired,
  labelRequired: bool,
  name: string.isRequired,
  rules: shape({}),
};

export default TextAreaControlled;
