/* eslint-disable */
import React from "react";
import { Box, Center, Modal } from "native-base";
import ButtonSubmit from "../../atoms/Button/ButtonSubmit";
import Icon from "../../atoms/Icon";
import { Description, Title } from "../../atoms/Text";
import LoadingScreen from "../LoadingScreen";

export function CoguiModal({ children, visible }) {
  return (
    <Modal
      _backdrop={{ opacity: 0.8, bg: "secondary.200" }}
      closeOnOverlayClick={false}
      isOpen={visible}
    >
      <Modal.Content maxWidth="400px" width="90%">
        <Modal.Body py="10" px="5">
          {children}
        </Modal.Body>
      </Modal.Content>
    </Modal>
  );
}

export const ModalContent = ({
  btnText,
  children,
  icon,
  message,
  onPress,
  title,
  visible,
}) => (
  <CoguiModal visible={visible}>
    <Center>
      {icon && (
        <Box mb={30}>
          <Icon icon={icon} />
        </Box>
      )}
      <Title text={title} />
      <Description text={message} marginBottom={6} />
      <ButtonSubmit text={btnText} onPress={onPress} />
      {children}
    </Center>
  </CoguiModal>
);

export const ModalLoader = ({ visible }) => (
  <CoguiModal visible={visible}>
    <LoadingScreen text="Espere por favor..." bg="" />
  </CoguiModal>
);
