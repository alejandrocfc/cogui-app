import React from "react";
import { number, string } from "prop-types";
import { Text, useTheme } from "native-base";

const commonProps = {
  color: string,
  marginBottom: number,
  text: string,
};

const defaultProps = {
  color: null,
  marginBottom: 0,
  text: "",
};

export function Title({ color, marginBottom, text, ...rest }) {
  const { colors } = useTheme();
  const textColor = color || colors.secondary["600"];
  return (
    <Text
      color={textColor}
      fontSize={17}
      bold
      fontFamily="Roboto"
      mb={marginBottom}
      {...rest}
    >
      {text}
    </Text>
  );
}
Title.propTypes = commonProps;
Title.defaultProps = defaultProps;

export function Description({ color, marginBottom, text, ...rest }) {
  const { colors } = useTheme();
  const textColor = color || colors.darkBlue["700"];
  return (
    <Text
      {...rest}
      color={textColor}
      fontSize={13}
      mb={marginBottom}
      fontFamily="GothamBook"
    >
      {text}
    </Text>
  );
}
Description.propTypes = commonProps;
Description.defaultProps = defaultProps;

export function Paragraph({ color, content, heading, ...rest }) {
  const { colors } = useTheme();
  const textColor = color || colors.darkBlue["700"];
  return (
    <Text {...rest} color={textColor} fontSize={13} fontFamily="GothamBook">
      {heading && <Text bold>{`${heading}: `}</Text>}
      {content}
    </Text>
  );
}
Paragraph.propTypes = {
  color: string,
  content: string.isRequired,
  heading: string,
};
Paragraph.defaultProps = {
  color: null,
  heading: null,
};
