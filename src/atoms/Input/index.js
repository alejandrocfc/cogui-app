import { FormControl, Input as InputNB, TextArea } from "native-base";
import React from "react";
import { Fonts } from "../../Theme";

export function Input(props) {
  return (
    <InputNB
      {...props}
      _input={{
        selectionColor: "#dbf4ff",
        cursorColor: "#004282",
        fontSize: 14,
        color: "#27272a",
        fontFamily: Fonts.GothamBook,
      }}
      focusOutlineColor="darkBlue.700"
      variant="underlined"
      mt={0}
      w="100%"
    />
  );
}

export function InputArea(props) {
  return (
    <TextArea
      {...props}
      _focus={{ bg: "transparent" }}
      focusOutlineColor="darkBlue.700"
      _input={{
        selectionColor: "#dbf4ff",
        cursorColor: "#004282",
        fontSize: 14,
        color: "#27272a",
        fontFamily: Fonts.GothamBook,
      }}
      h={20}
      placeholder=""
      mt={0}
      px={5}
      w="100%"
      borderWidth="2"
      borderColor="coolGray.300"
    />
  );
}

export function InputLabel({ children }) {
  return (
    <FormControl.Label
      _text={{
        textAlign: "left",
        color: "blue.700",
        fontFamily: Fonts.GothamBook,
        fontSize: 14,
        letterSpacing: 0.3,
        lineHeight: 24,
      }}
    >
      {children}
    </FormControl.Label>
  );
}
