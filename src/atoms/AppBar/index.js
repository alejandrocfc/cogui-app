/* eslint-disable react-hooks/rules-of-hooks */
import { bool, func, string } from "prop-types";
import React from "react";
import {
  Box,
  ChevronLeftIcon,
  Heading,
  HStack,
  IconButton,
  StatusBar,
  useTheme,
} from "native-base";
import { Title } from "../Text";
import { useNavigate } from "react-router-dom";

function AppBar({ children, hasSafeAreaTop }) {
  const { colors } = useTheme();
  return (
    <Box>
      <StatusBar
        backgroundColor={colors.secondary["600"]}
        barStyle="light-content"
      />
      {hasSafeAreaTop && (
        <Box safeAreaTop backgroundColor={colors.secondary["600"]} />
      )}
      <HStack
        borderBottomColor="red.500"
        borderBottomWidth={1}
        bg="secondary.600"
        px="1"
        py="1"
        justifyContent="space-between"
        alignItems="center"
      >
        {children}
      </HStack>
    </Box>
  );
}
AppBar.defaultProps = {
  hasSafeAreaTop: false,
};
AppBar.propTypes = {
  hasSafeAreaTop: bool,
};

AppBar.Title = function ({ title }) {
  const { colors } = useTheme();
  return (
    <HStack alignItems="center">
      <Heading textAlign="center">
        <Title color={colors.lightText} text={title} />
      </Heading>
    </HStack>
  );
};
AppBar.Title.propTypes = { title: string.isRequired };

AppBar.Content = function ({ children, ...args }) {
  return (
    <HStack alignItems="center" {...args}>
      {children}
    </HStack>
  );
};

AppBar.Back = function ({ onPress, route }) {
  const navigate = useNavigate();
  return (
    <HStack space="1" alignItems="center">
      <IconButton
        _pressed={{
          bg: "transparent",
        }}
        onPress={() => (onPress ? onPress() : navigate(route))}
        icon={<ChevronLeftIcon size={5} color="lightText" />}
      />
    </HStack>
  );
};
AppBar.Back.defaultProps = { onPress: null };
AppBar.Back.propTypes = { onPress: func };

export default AppBar;
