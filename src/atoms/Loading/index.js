import React from "react";
import { oneOf } from "prop-types";
import { Spinner, useTheme } from "native-base";

function Loading({ size }) {
  const { colors } = useTheme();
  return <Spinner size={size} color={colors.indigo["50"]} />;
}

Loading.defaultProps = {
  size: "large",
};

Loading.propTypes = {
  size: oneOf(["large", "small"]),
};

export default Loading;
