import { Button, VStack } from "native-base";
import { bool, func, oneOfType, shape, string } from "prop-types";
import { Forms } from "../../Theme";

function Footer({
  btnPrimary,
  btnPrimaryProps,
  btnPrimaryText,
  btnSecondary,
  btnSecondaryText,
}) {
  return (
    <VStack
      alignItems="center"
      bg="light.100"
      borderTopRadius={20}
      safeAreaBottom
      shadow={6}
      space={2}
      pt={2}
      minH={btnPrimaryText && btnSecondaryText ? 110 : 60}
    >
      {btnPrimaryText && (
        <Button
          borderRadius="28"
          width="90%"
          colorScheme="secondary"
          onPress={btnPrimary}
          _text={{ ...Forms.defaultPrimaryButtonText }}
          {...btnPrimaryProps}
        >
          {btnPrimaryText}
        </Button>
      )}
      {btnSecondaryText && (
        <Button
          borderRadius="28"
          width="90%"
          variant="outline"
          colorScheme="secondary"
          onPress={btnSecondary}
          _text={{ ...Forms.defaultSecondaryButtonText }}
        >
          {btnSecondaryText}
        </Button>
      )}
    </VStack>
  );
}

Footer.defaultProps = {
  btnSecondary: null,
  btnSecondaryText: false,
  btnPrimaryProps: null,
};
Footer.propTypes = {
  btnPrimary: func.isRequired,
  btnPrimaryProps: shape({}),
  btnPrimaryText: string.isRequired,
  btnSecondary: func,
  btnSecondaryText: oneOfType([string, bool]),
};

export default Footer;
