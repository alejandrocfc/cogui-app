import { Fonts } from "../../Theme";

export default {
  container: {
    fontSize: 14,
    color: "#27272a",
    fontFamily: Fonts.GothamBook,
  },
};
