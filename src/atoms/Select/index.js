import React from "react";
import { arrayOf, bool, shape, string } from "prop-types";
import {
  ChevronDownIcon,
  ChevronUpIcon,
  FormControl,
  Select,
  WarningOutlineIcon,
} from "native-base";
import { Controller } from "react-hook-form";
import { InputLabel } from "../Input";
import styles from "./Select.styles";

function CoguiSelect({
  control,
  controlledProps,
  defaultValue,
  disabled,
  hasError,
  label,
  name,
  options,
  required,
  rules,
}) {
  return (
    <FormControl
      isRequired={required}
      isInvalid={hasError}
      isDisabled={disabled}
      {...controlledProps}
    >
      <InputLabel>{label}</InputLabel>
      <Controller
        name={name}
        control={control}
        defaultValue={defaultValue}
        rules={rules}
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <>
            <Select
              placeholder="Seleccione"
              selectedValue={value}
              mt={0}
              disabled={disabled}
              style={styles.container}
              borderRadius="md"
              w="100%"
              onValueChange={onChange}
              dropdownOpenIcon={<ChevronUpIcon size="5" mr="2" />}
              dropdownCloseIcon={<ChevronDownIcon size="5" mr="2" />}
            >
              {options.map((option, index) => (
                <Select.Item
                  key={index}
                  value={option.value}
                  label={option.label}
                />
              ))}
            </Select>
            {!!error && (
              <FormControl.ErrorMessage
                leftIcon={<WarningOutlineIcon size="xs" />}
              >
                {error.message}
              </FormControl.ErrorMessage>
            )}
          </>
        )}
      />
    </FormControl>
  );
}

CoguiSelect.propTypes = {
  control: shape({}).isRequired,
  controlledProps: shape({}),
  defaultValue: string,
  disabled: bool,
  hasError: bool,
  label: string.isRequired,
  name: string.isRequired,
  options: arrayOf(shape({ value: string, label: string })),
  required: bool,
  rules: shape({}),
};

CoguiSelect.defaultProps = {
  controlledProps: {},
  defaultValue: "",
  disabled: false,
  hasError: false,
  options: [],
  required: false,
  rules: {},
};

export default CoguiSelect;
