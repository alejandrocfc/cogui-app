import React from 'react';
import {Button} from 'native-base';

function CoguiButton({children, ...props}) {
  return <Button {...props}>{children}</Button>;
}

export default CoguiButton;
