import React from "react";
import { bool, func, string } from "prop-types";
import { Button } from "native-base";
import { Forms } from "../../Theme";

function ButtonSubmit({ loading, onPress, text }) {
  return (
    <Button
      isLoading={loading}
      colorScheme="secondary"
      onPress={onPress}
      h={47}
      _text={{ ...Forms.defaultPrimaryButtonText }}
      w="90%"
      borderRadius={28}
    >
      {text}
    </Button>
  );
}

ButtonSubmit.defaultProps = {
  loading: false,
  onPress: () => undefined,
  text: "",
};
ButtonSubmit.propTypes = {
  loading: bool,
  onPress: func,
  text: string,
};

export default ButtonSubmit;
