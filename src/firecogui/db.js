import {
  addDoc,
  collection,
  doc,
  enableIndexedDbPersistence,
  getDoc,
  getDocs,
  getFirestore,
  onSnapshot,
  orderBy,
  query,
  updateDoc,
  where,
} from "firebase/firestore";
import {
  getDownloadURL,
  getMetadata,
  getStorage,
  listAll,
  ref,
  uploadBytes,
} from "firebase/storage";
import { useEffect } from "react";
import firebaseConfig from "./config";
import { cleanEmptyFields } from "../helpers/mapper";

export const db = getFirestore(firebaseConfig);
const Storage = getStorage(firebaseConfig);

enableIndexedDbPersistence(db).catch((err) => {
  console.log(err);
});

export const getRecords = async (path) => {
  try {
    console.log("HERE");
    const querySnapshot = await getDocs(collection(db, path));
    if (querySnapshot.empty) return [];
    const list = [];
    querySnapshot.forEach((doc) => {
      list.push({ id: doc.id, data: doc.data() });
    });
    return list;
  } catch (e) {
    console.log(e);
    return e;
  }
};
export const getRecord = async (path, order, ...res) => {
  try {
    const q = query(
      collection(db, path),
      where(...res),
      orderBy(order, "desc")
    );
    const querySnapshot = await getDocs(q);
    const document = [];
    if (querySnapshot.empty) {
      return document;
    }
    querySnapshot.forEach((doc) => {
      document.push({ id: doc.id, ...doc.data() });
    });
    return document;
  } catch (e) {
    console.log(e);
    return [];
  }
};

export const getDatesByToday = async () => {
  try {
    const start = new Date();
    start.setHours(0, 0, 0, 0);
    const end = new Date(start.getTime());
    end.setHours(23, 59, 59, 999);
    const q = query(
      collection(db, "/dates"),
      where("date", ">=", start),
      where("date", "<=", end),
      where("state", "==", "active"),
      orderBy("date", "asc")
    );
    const querySnapshot = await getDocs(q);
    const document = [];
    if (querySnapshot.empty) {
      return document;
    }
    const list = [];
    querySnapshot.forEach((doc) => {
      list.push({ id: doc.id, ...doc.data() });
    });
    for await (const doc of list) {
      const pet = await getRecordById("pets", doc.idPet);
      const owner = await getRecordById("owners", pet.owner);
      doc.pet = pet;
      doc.owner = owner;
      document.push({ id: doc.id, ...doc });
    }
    return document;
  } catch (e) {
    console.log(e);
    return [];
  }
};

export const getRecordById = async (path, id) => {
  try {
    const docRef = doc(db, path, id);
    const docSnap = await getDoc(docRef);
    if (docSnap.exists()) {
      return docSnap.data();
    }
    return false;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const saveRecord = async (path, data) => {
  try {
    const docRef = await addDoc(collection(db, path), cleanEmptyFields(data));
    return docRef.id;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const updateRecord = async (data, path, docId) => {
  try {
    const docRef = doc(db, path, docId);
    await updateDoc(docRef, cleanEmptyFields(data));
  } catch (e) {
    console.log(e);
    throw e;
  }
};

export const getFiles = async (path) => {
  try {
    const listRef = ref(Storage, path);
    const { items } = await listAll(listRef);
    const list = [];
    for await (const item of items) {
      const url = await getDownloadURL(item);
      const data = await getMetadata(item);
      list.push({ data, url });
    }
    return list;
  } catch (e) {
    console.log(e);
    return [];
  }
};
export const uploadFile = async (files, path) => {
  const generatedResponse = [];
  for (const file of files) {
    try {
      const storageRef = ref(Storage, `${path}${file.name}`);
      const insertResponse = await uploadBytes(storageRef, file);
      // and response need to be added into final response array
      generatedResponse.push(insertResponse);
    } catch (error) {
      console.log(`error${error}`);
    }
  }
  console.log("complete all"); // gets loged first
  return generatedResponse; // return without waiting for process of
};

export function useInitFirebaseSubscriptions(
  path,
  setList,
  setData,
  setLoading
) {
  useEffect(() => {
    console.log("[CONTROLLER]: subscribed to Firebase");
    setLoading(true);
    const unsubscribe = onSnapshot(
      collection(db, path),
      (querySnapshot) => {
        if (querySnapshot.empty) return [];
        const list = [];
        querySnapshot.forEach((doc) => {
          list.push({ id: doc.id, data: doc.data() });
        });
        setLoading(false);
        setList(list);
        setData(list);
      },
      (error) => console.log(error)
    );
    return () => {
      unsubscribe();
      console.log("[CONTROLLER]: unsubscribed from Firebase");
    };
  }, []);
}
