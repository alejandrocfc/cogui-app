import {
  getAuth,
  signInWithPopup,
  signOut,
  GoogleAuthProvider,
  setPersistence,
  browserLocalPersistence,
} from "firebase/auth";
import firebaseConfig from "./config";

const provider = new GoogleAuthProvider();

const auth = getAuth(firebaseConfig);

const logIn = async () => {
  try {
    await setPersistence(auth, browserLocalPersistence);
    const result = await signInWithPopup(auth, provider);
    return result.user;
  } catch (error) {
    return error.message;
  }
};

const logOut = async (cb) => {
  await signOut(auth);
  cb();
};

export default { auth, logIn, logOut };
