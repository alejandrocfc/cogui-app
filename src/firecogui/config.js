import { initializeApp } from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCi0Jwz0yWuxf5KScnRF126htpgiCXbk4Q",
  authDomain: "cogui-app.firebaseapp.com",
  projectId: "cogui-app",
  storageBucket: "cogui-app.appspot.com",
  messagingSenderId: "614737839605",
  appId: "1:614737839605:web:a73cc02a2b085ecacbccbc",
};

const app = initializeApp(firebaseConfig);

export default app;
