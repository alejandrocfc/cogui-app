import { Center, CheckIcon, Column, HStack, Icon } from "native-base";
import React, { useEffect, useState } from "react";
import { bool, shape } from "prop-types";
import { useCoguiContext } from "../../context";
import { checkNested } from "../../helpers/mapper";
import Inputcontrolled from "../../molecules/Form/inputcontrolled";
import DateControlled from "../../molecules/Form/datecontrolled";
import CoguiSelect from "../../atoms/Select";
import { Description, Paragraph } from "../../atoms/Text";
import Card from "../../molecules/Card";
import Location from "../../molecules/Location";
import SearchBar from "../../molecules/SearchBar";
import { Fonts } from "../../Theme";
import { MdSearch } from "react-icons/md";

const commonProps = {
  control: shape({}).isRequired,
  errors: shape({}).isRequired,
};

const formPet = [
  { label: "Nombre", errorText: "El nombre es requerido", name: "pet.name" },
  { label: "Especie", errorText: "La especie es requerida", name: "pet.kind" },
  { label: "Raza", errorText: "La raza es requerida", name: "pet.breed" },
];

const formSlave = [
  {
    label: "Nombre",
    name: "owner.name",
    inputProps: {},
    rules: {
      required: "El nombre es requerido",
      minLength: { value: 3, message: "El nombre debe tener mínimo 3 letras" },
    },
  },
  {
    label: "Celular",
    name: "owner.tel",
    inputProps: { keyboardType: "phone-pad" },
    rules: { required: "El número de celular es requerido" },
  },
  {
    label: "Dirección",
    name: "owner.address",
    inputProps: {},
    rules: {
      required: "La dirección es requerida",
      minLength: {
        value: 3,
        message: "La dirección debe tener mínimo 3 letras",
      },
    },
  },
];

export function PetStep({ errors, control, showHint }) {
  return (
    <>
      {showHint && (
        <Paragraph content="Por favor ingrese los datos del paciente (mascota)." />
      )}
      {formPet.map(({ errorText, name, label }) => (
        <Inputcontrolled
          key={name}
          hasError={checkNested(errors, ...name.split("."))}
          controlledProps={{ mt: "4" }}
          labelRequired
          control={control}
          label={label}
          name={name}
          rules={{ required: errorText }}
        />
      ))}
      <CoguiSelect
        control={control}
        controlledProps={{ mt: "4" }}
        name="pet.sex"
        hasError={checkNested(errors, "pet", "sex")}
        label="Sexo"
        required
        rules={{ required: "Sexo requerido" }}
        options={[
          { value: "F", label: "Hembra" },
          { value: "M", label: "Macho" },
        ]}
      />
      <DateControlled
        name="pet.age"
        control={control}
        controlledProps={{ mt: "4" }}
        label="Edad"
      />
    </>
  );
}
PetStep.defaultProps = {
  showHint: false,
};
PetStep.propTypes = {
  ...commonProps,
  showHint: bool,
};

export function SlaveStep({ control, errors, isEdit, resetField, setValue }) {
  const [enterType, setEnterType] = useState(isEdit ? "add" : null);
  const getContexText = () =>
    enterType === "add" ? "ingrese los datos del" : "seleccione un";
  const switchScreen = () => {
    setEnterType((prevState) => (prevState === "add" ? "search" : "add"));
  };
  return (
    <>
      {enterType !== null && !isEdit && (
        <Paragraph mb="2" content={`Por favor ${getContexText()} esclavo.`} />
      )}
      {enterType === "add" && (
        <SlaveAdd
          control={control}
          errors={errors}
          isEdit={isEdit}
          resetField={resetField}
          addValue={setValue}
          onSearch={() => setEnterType("search")}
        />
      )}
      {enterType === "search" && (
        <SlaveSearch
          onAdd={() => setEnterType("add")}
          resetField={resetField}
          addValue={setValue}
        />
      )}
      {enterType !== null && !isEdit && (
        <SlaveSeparator switchScreen={switchScreen} type={enterType} />
      )}
      {enterType === null && (
        <HStack space={3} justifyContent="center">
          <ActionButton
            title="Buscar esclavo"
            BtnIcon={() => <MdSearch />}
            onPress={() => setEnterType("search")}
          />
          <ActionButton
            title="Agregar esclavo"
            BtnIcon={() => <MdSearch />}
            onPress={() => setEnterType("add")}
          />
        </HStack>
      )}
    </>
  );
}
SlaveStep.propTypes = {
  ...commonProps,
};

function ActionButton({ title, BtnIcon, onPress }) {
  return (
    <Center
      h="20"
      rounded="md"
      px="2"
      borderWidth="1"
      borderColor="darkBlue.700"
    >
      <div onClick={onPress}>
        <Center>
          <Icon color="darkBlue.700" as={<BtnIcon />} size="8" />
        </Center>
        <Description text={title} />
      </div>
    </Center>
  );
}

function SlaveAdd({ addValue, control, errors, isEdit }) {
  return (
    <>
      {formSlave.map(({ name, label, rules, inputProps }) => (
        <Inputcontrolled
          key={name}
          hasError={checkNested(errors, ...name.split("."))}
          controlledProps={{ mt: "4" }}
          labelRequired
          control={control}
          inputProps={inputProps}
          label={label}
          name={name}
          rules={rules}
        />
      ))}
      {isEdit && <Location addValue={addValue} />}
    </>
  );
}

function SlaveSearch({ resetField, addValue }) {
  const [value, setValue] = useState("");
  const [loading, setLoading] = useState(false);
  const [empty, setEmpty] = useState(false);
  const [selected, setSelected] = useState(false);
  const [list, setList] = useState([]);
  const [controller] = useCoguiContext();
  useEffect(() => {
    if (value.length >= 3) {
      setLoading(true);
      const records = controller.owners.filter(
        (obj) => obj.name.search(new RegExp(value, "i")) >= 0
      );
      setList(records);
      setEmpty(records.length <= 0);
      setLoading(false);
    } else {
      setLoading(false);
      setSelected(null);
      setEmpty(false);
      setList([]);
    }
  }, [value]);
  const clearValues = () => {
    setSelected(null);
    setValue("");
    resetField("pet.owner");
    resetField("owner.name");
    resetField("owner.tel");
    resetField("owner.address");
  };
  const onClear = () => {
    setList([]);
    setLoading(false);
    setEmpty(false);
    clearValues();
  };
  const handleSelect = ({ id, ...data }) => {
    if (selected === id) {
      return clearValues();
    }
    setSelected(id);
    addValue("pet.owner", id);
    addValue("owner.name", data.name);
    addValue("owner.tel", data.tel);
    addValue("owner.address", data.address);
  };
  return (
    <>
      <SearchBar
        loader={loading}
        handleChange={setValue}
        value={value}
        handleClear={onClear}
      />
      {list.length > 0 && (
        <Center>
          {list.map((item) => (
            <div key={item.id} onClick={() => handleSelect(item)}>
              <Card>
                <HStack space={2}>
                  <Paragraph content={item.name} />
                  {selected === item.id && (
                    <CheckIcon size="5" mt="0.5" color="emerald.500" />
                  )}
                </HStack>
              </Card>
            </div>
          ))}
        </Center>
      )}
      {empty && <Paragraph color="muted.700" content="No hay esclavos" />}
    </>
  );
}

function SlaveSeparator({ switchScreen, type }) {
  return (
    <Center>
      <Column justifyContent="center" mt="2">
        <Column
          h="1"
          borderBottomWidth="1"
          borderBottomColor="darkBlue.600"
          mt="2"
        />
        <Center mt="2">
          <div onClick={switchScreen}>
            <Paragraph
              mx="10"
              fontFamily={Fonts.GothamMedium}
              content={`O ${type === "search" ? "agregar " : "buscar "}esclavo`}
            />
          </div>
        </Center>
      </Column>
    </Center>
  );
}
