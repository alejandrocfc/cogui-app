import React, { useState } from "react";
import { useForm } from "react-hook-form";
import usePet from "../../hooks/usePet";
import AppBar from "../../atoms/AppBar";
import Footer from "../../atoms/Footer";
import Layout from "../../molecules/Layout";
import SummaryStep from "../../molecules/SummaryStep";
import { PetStep, SlaveStep } from "./formSteps";
import { useLocation, useNavigate } from "react-router-dom";

const getSubmitText = (step, isEdit) =>
  step === 2 || isEdit ? "Guardar" : "Siguiente";
const getTitle = (isEdit, entity) => {
  if (!isEdit) {
    return "Nuevo paciente";
  } else {
    return `Editar ${entity === "pets" ? "paciente" : "esclavo"}`;
  }
};

function PetForm() {
  const {
    state: { entity, initialStep, initialValues },
  } = useLocation();
  const [step, setStep] = useState(initialStep || 0);
  const isEdit = !!initialValues.id;
  const navigate = useNavigate();
  const closeNotification = () => navigate(-1);
  const { createPet, updateEntity } = usePet({
    closeNotification,
    notification: {
      title: isEdit ? "Información actualizada!" : "Paciente creado!",
      message: isEdit ? "" : "El paciente fue creado correctamente.",
    },
  });
  const {
    control,
    getValues,
    handleSubmit,
    setValue,
    resetField,
    formState: { errors },
  } = useForm({ defaultValues: initialValues });

  const onSubmit = async (values) => {
    if (step <= 1 && !isEdit) {
      setStep((prevState) => prevState + 1);
    } else {
      console.log(values);
      /*isEdit
        ? await updateEntity(entity, initialValues.id, values)
        : await createPet(values);*/
    }
  };

  return (
    <Layout
      onPress={closeNotification}
      isForm
      title={getTitle(isEdit, entity)}
      rightComp={<AppBar.Title title="" />}
      footer={
        <Footer
          btnPrimaryText={getSubmitText(step, isEdit)}
          btnPrimary={handleSubmit(onSubmit)}
          btnSecondaryText={step >= 1 && !isEdit && "Atrás"}
          btnSecondary={() => setStep((prevState) => prevState - 1)}
        />
      }
    >
      {step === 0 && (
        <PetStep showHint={!isEdit} control={control} errors={errors} />
      )}
      {step === 1 && (
        <SlaveStep
          control={control}
          errors={errors}
          isEdit={isEdit}
          resetField={resetField}
          setValue={setValue}
        />
      )}
      {step === 2 && <SummaryStep values={getValues()} />}
    </Layout>
  );
}

export default PetForm;
