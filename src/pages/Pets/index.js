import React, { useEffect, useState } from "react";
import { Box, Fab, Icon } from "native-base";
import { MdAdd } from "react-icons/md";
import { useCoguiContext } from "../../context";
import { PET_CLEAR, PET_SET } from "../../context/types";
import LoadingScreen from "../../molecules/LoadingScreen";
import SearchBar from "../../molecules/SearchBar";
import List from "../../molecules/List";
import { useNavigate } from "react-router-dom";

const mapPets = (querySnapshot, owners) => {
  const pets = [];
  querySnapshot.forEach(({ id, ...doc }) => {
    const pet = doc;
    const owner = owners.find((i) => i.id === pet.owner) || {};
    pets.push({ id, pet, owner });
  });
  return Promise.resolve(pets);
};
function Pets() {
  const [value, setValue] = useState("");
  const [loader, setLoader] = useState(false);
  const [loading, setLoading] = useState(true);
  const [pets, setPets] = useState([]);
  const [data, setData] = useState([]);
  const [controller, dispatch] = useCoguiContext();
  const navigate = useNavigate();

  useEffect(() => {
    if (pets.length > 0 && data.length > 0) {
      setLoading(false);
    }
  }, [pets, data]);

  useEffect(() => {
    if (controller.pets.length > 0 && controller.owners.length > 0) {
      (async () => {
        const petsResult = await mapPets(controller.pets, controller.owners);
        setPets(petsResult);
        setData(petsResult);
      })();
    }
  }, [controller.pets, controller.owners]);

  useEffect(() => {
    setValue("");
    dispatch({ type: PET_CLEAR });
  }, []);

  const onClear = () => {
    setPets(data);
    setValue("");
    setLoader(false);
  };

  useEffect(() => {
    if (value.length >= 3) {
      setLoader(true);
      const records = data.filter(
        (obj) => obj.pet.name.search(new RegExp(value, "i")) >= 0
      );
      setPets(records);
      setLoader(false);
    } else if (!value) {
      onClear();
    }
  }, [value]);

  const goDetail = (id) => {
    dispatch({
      type: PET_SET,
      value: data.find((pets) => pets.id === id),
    });
    navigate(`/paciente/${id}`);
  };

  const newPet = () => {
    navigate("/formulario-paciente", {
      state: { initialValues: { pet: {}, owner: {} } },
    });
  };

  if (loading) {
    return <LoadingScreen />;
  }

  return (
    <>
      <Box pt="4" px="4" h="100%" overflowY="scroll">
        <SearchBar
          value={value}
          handleChange={setValue}
          handleClear={onClear}
          loader={loader}
        />
        <List
          data={pets.map((doc) => ({
            id: doc.id,
            title: doc.pet.name,
            subtitle: `Esclavo: ${doc.owner.name}`,
          }))}
          onRowPress={goDetail}
        />
      </Box>
      <Fab
        renderInPortal={false}
        onPress={newPet}
        bg="darkBlue.700"
        color="white"
        shadow={6}
        bottom={10}
        icon={<Icon color="white" as={MdAdd} />}
      />
    </>
  );
}

export default Pets;
