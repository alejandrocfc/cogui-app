import { Container, Heading, Icon, IconButton } from "native-base";
import Auth from "../../firecogui/auth";
import { Path } from "react-native-svg";
import { useCoguiContext } from "../../context";
import { USER_LOGIN_OUT } from "../../context/types";

function Home() {
  const [, dispatch] = useCoguiContext();

  const handleLogout = async () => {
    await Auth.logOut(() => {
      dispatch({ type: USER_LOGIN_OUT });
    });
  };
  return (
    <Container>
      <IconButton
        onPress={handleLogout}
        icon={
          <Icon size="6">
            <Path d="M5 21q-.825 0-1.413-.587Q3 19.825 3 19V5q0-.825.587-1.413Q4.175 3 5 3h7v2H5v14h7v2Zm11-4-1.375-1.45 2.55-2.55H9v-2h8.175l-2.55-2.55L16 7l5 5Z" />
          </Icon>
        }
      />
      <Heading>A component library for the</Heading>
    </Container>
  );
}
export default Home;
