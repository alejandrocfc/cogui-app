import { useEffect } from "react";
import { Box, Center } from "native-base";
import { useNavigate } from "react-router-dom";
import Auth from "../../firecogui/auth";
import { useLottie } from "lottie-react";
import coguiAnimation from "../../assets/cogui.json";
import { useCoguiContext } from "../../context";
import { VIEWER } from "../Authorization/roles";
import ButtonSubmit from "../../atoms/Button/ButtonSubmit";

function Login() {
  const [controller] = useCoguiContext();
  const navigate = useNavigate();
  const options = {
    animationData: coguiAnimation,
    loop: true,
  };

  const { View } = useLottie(options);

  useEffect(() => {
    if (controller.role === VIEWER) {
      navigate("/", { replace: true });
    }
  }, [controller.role]);
  const handleLogin = async () => {
    await Auth.logIn();
    navigate("/", { replace: true });
  };
  return (
    <Center flex="1" flexDir="column" w="100%">
      <Center>
        <Box pt="2" pb="5" mb="8">
          {View}
        </Box>
        <ButtonSubmit onPress={handleLogin} text="INGRESAR" />
      </Center>
    </Center>
  );
}

export default Login;
