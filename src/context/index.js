import React, { createContext, useContext, useReducer } from "react";
import { node } from "prop-types";
import { isADateInstance } from "../helpers/mapper";
import { LOGOUT, VIEWER } from "../pages/Authorization/roles";
import {
  NOTIFY_SUCCESS,
  NOTIFY_ERROR,
  NOTIFY_EXPIRED,
  NOTIFY_CLEAN,
  NOTIFY_WARNING,
  NOTIFY_LOADER,
  OWNERS_SET,
  PET_CLEAR,
  PET_SET,
  PETS_SET,
  SET_INTERNET,
  USER,
  USER_LOGIN_IN,
  USER_LOGIN_OUT,
} from "./types";

const CoguiContext = createContext();

const initialState = {
  user: {
    name: "",
    document: null,
    number: null,
    email: "",
  },
  isConnected: true,
  role: "",
  notification: { loading: false, type: "", content: {} },
  owners: [],
  pets: [],
  pet: {
    pet: {},
    owner: {},
  },
};

const reducer = (state, action) => {
  switch (action.type) {
    case USER: {
      return { ...state, user: action.value };
    }
    case PET_SET: {
      const info = { ...action.value };
      if (!isADateInstance(action.value.pet.age)) {
        info.pet.age = action.value.pet.age.toDate();
      }
      return { ...state, pet: action.value };
    }
    case OWNERS_SET: {
      return { ...state, owners: action.value };
    }
    case PETS_SET: {
      return { ...state, pets: action.value };
    }
    case PET_CLEAR: {
      return { ...state, pet: initialState.pet };
    }
    case USER_LOGIN_IN: {
      return { ...state, role: VIEWER };
    }
    case USER_LOGIN_OUT: {
      return { ...state, role: LOGOUT };
    }
    case SET_INTERNET: {
      return { ...state, isConnected: action.value };
    }
    case NOTIFY_SUCCESS:
    case NOTIFY_WARNING:
    case NOTIFY_ERROR: {
      return {
        ...state,
        notification: {
          ...state.notification,
          type: action.type,
          content: action.value,
          loading: false,
        },
      };
    }
    case NOTIFY_LOADER: {
      return {
        ...state,
        notification: {
          ...state.notification,
          loading: true,
        },
      };
    }
    case NOTIFY_EXPIRED: {
      return {
        ...state,
        notification: {
          ...state.notification,
          logout: true,
        },
      };
    }
    case NOTIFY_CLEAN: {
      return {
        ...state,
        notification: {
          ...initialState.notification,
        },
      };
    }
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};

const AppContextProvider = function ({ children }) {
  const [controller, dispatch] = useReducer(reducer, initialState);
  return (
    <CoguiContext.Provider value={[controller, dispatch]}>
      {children}
    </CoguiContext.Provider>
  );
};
AppContextProvider.propTypes = {
  children: node.isRequired,
};

const useCoguiContext = () => useContext(CoguiContext);

export { AppContextProvider, useCoguiContext, initialState };
