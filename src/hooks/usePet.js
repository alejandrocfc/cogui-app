import { useEffect, useState } from "react";
import { useCoguiContext } from "../context";
import {
  NOTIFY_CLEAN,
  NOTIFY_ERROR,
  NOTIFY_LOADER,
  NOTIFY_SUCCESS,
  PET_SET,
} from "../context/types";
import { saveRecord, updateRecord } from "../firecogui/db";
import { generateId, setTimeDate2Date } from "../helpers/mapper";

export default function usePet({ closeNotification, notification }) {
  const [, dispatch] = useCoguiContext();
  const [shouldReturn, setReturn] = useState(false);

  useEffect(() => {
    if (shouldReturn) {
      dispatch({
        type: NOTIFY_SUCCESS,
        value: {
          title: notification.title,
          message: notification.message,
          btnText: "Continuar",
          onPress: () => {
            dispatch({ type: NOTIFY_CLEAN });
            closeNotification();
          },
        },
      });
    }
  }, [shouldReturn]);
  const createPet = async (values) => {
    try {
      dispatch({ type: NOTIFY_LOADER });
      const { pet, owner } = { ...values };
      if (!pet.owner) {
        const data = { ...pet };
        const ownerId = generateId();
        data.owner = ownerId;
        if (!navigator.onLine) {
          saveRecord("owners", owner, ownerId);
          saveRecord("pets", data);
          setTimeout(() => {
            setReturn(true);
          }, 2500);
        } else {
          await saveRecord("owners", owner, ownerId);
          await saveRecord("pets", data);
          setReturn(true);
        }
      } else {
        if (!navigator.onLine) {
          saveRecord("pets", pet);
          setTimeout(() => {
            setReturn(true);
          }, 2500);
        } else {
          await saveRecord("pets", pet);
          setReturn(true);
        }
      }
    } catch (e) {
      dispatch({
        type: NOTIFY_ERROR,
        value: {
          title: "Paciente no creado!",
          message: "Error al crear el paciente",
          btnText: "Intentar de nuevo",
          onPress: () => {
            dispatch({ type: NOTIFY_CLEAN });
          },
        },
      });
    }
  };
  const updateEntity = async (entity, petId, values) => {
    dispatch({ type: NOTIFY_LOADER });
    const { pet, owner } = { ...values };
    const docId = entity === "pets" ? petId : values.pet.owner;
    const data = entity === "pets" ? pet : owner;
    dispatch({
      type: PET_SET,
      value: values,
    });
    if (!navigator.onLine) {
      updateRecord(data, entity, docId);
      setTimeout(() => {
        setReturn(true);
      }, 2500);
    } else {
      await updateRecord(data, entity, docId);
      setReturn(true);
    }
  };

  const createDate = async (values, isPast, idPet, pet) => {
    try {
      const idDate = generateId();
      dispatch({ type: NOTIFY_LOADER });
      const data = {
        type: values.type,
        color: values.color,
        note: values.note,
        data: {},
        date: setTimeDate2Date(values.date, values.time),
        state: isPast ? "closed" : "active",
        idPet,
      };
      if (values.type === "appointment" && isPast) {
        data.data = {
          appointment: {
            anamnesis: values.anamnesis,
            signs: values.signs,
            treatment: values.treatment,
          },
          exam: {
            spine: values.spine,
            limb: values.limb,
            sensibility: values.sensibility,
            kinaesthesia: values.kinaesthesia,
          },
        };
      }
      if (!navigator.onLine) {
        saveRecord("dates", data, idDate);
        setTimeout(async () => {
          // dont throw if notification doesnt save
          setReturn(true);
        }, 2500);
      } else {
        // dont throw if notification doesnt save
        await saveRecord("dates", data, idDate);
        setReturn(true);
      }
    } catch (e) {
      console.log(e);
      dispatch({
        type: NOTIFY_ERROR,
        value: {
          title: "Ops!",
          message: "Cita no creada",
          btnText: "Intentar de nuevo",
          onPress: () => {
            dispatch({ type: NOTIFY_CLEAN });
          },
        },
      });
    }
  };

  const updateDate = async (values, data, fromHome) => {
    try {
      dispatch({ type: NOTIFY_LOADER });
      const info = {
        description: values.description,
        state: fromHome ? "closed" : null,
      };
      if (data.type === "appointment") {
        info.data = {
          appointment: {
            anamnesis: values.anamnesis,
            signs: values.signs,
            treatment: values.treatment,
          },
          exam: {
            spine: values.spine,
            limb: values.limb,
            sensibility: values.sensibility,
            kinaesthesia: values.kinaesthesia,
          },
        };
      }
      if (!navigator.onLine) {
        updateRecord(info, "dates", data.id);
        setTimeout(() => {
          setReturn(true);
        }, 2500);
      } else {
        await updateRecord(info, "dates", data.id);
        setReturn(true);
      }
    } catch (e) {
      console.log(e);
      dispatch({
        type: NOTIFY_ERROR,
        value: {
          title: "Ops!",
          message: "Cita no modificada",
          btnText: "Intentar de nuevo",
          onPress: () => {
            dispatch({ type: NOTIFY_CLEAN });
          },
        },
      });
    }
  };

  const reformDate = async (info, idDate) => {
    try {
      dispatch({ type: NOTIFY_LOADER });
      if (!navigator.onLine) {
        updateRecord(info, "dates", idDate);
        setTimeout(() => {
          setReturn(true);
        }, 2500);
      } else {
        await updateRecord(info, "dates", idDate);
        setReturn(true);
      }
    } catch (e) {
      console.log(e);
      dispatch({
        type: NOTIFY_ERROR,
        value: {
          title: "Ops!",
          message: "Cita no creada",
          btnText: "Intentar de nuevo",
          onPress: () => {
            dispatch({ type: NOTIFY_CLEAN });
          },
        },
      });
    }
  };

  return {
    createDate,
    createPet,
    reformDate,
    updateDate,
    updateEntity,
  };
}
